import {
    SiReact,
    SiJavascript,
    SiNextdotjs,
    SiTailwindcss,
    SiTypescript,
    SiPhp,
    SiSymfony,
    SiMysql,
    SiPrisma,
    SiGit,
} from "react-icons/si";
export default function Stack() {
    return (
        <>
            <h2 id="stack" className="font-sans text-center font-extrabold text-3xl text-solidheadinglight dark:text-solidheadingdark">
                Ma Stack Technique
            </h2>
            <div className="flex overflow-hidden md:text-9xl text-6xl  space-x-16 group">
                <div className="flex space-x-16 loop-scroll group-hover:paused">
                    <SiJavascript className="hover:text-jscolor duration-300"/>
                    <SiTypescript className="hover:text-tscolor duration-300"/>
                    <SiReact className="hover:text-reactcolor duration-300"/>
                    <SiTailwindcss className="hover:text-tailwindcolor duration-300"/>
                    <SiGit className="hover:text-gitcolor duration-300"/>
                </div>
                <div className="flex space-x-16 loop-scroll md:text-9xl text-6xl group-hover:paused">
                    <SiPhp className="hover:text-phpcolor duration-300"/>
                    <SiNextdotjs/>
                    <SiSymfony/>
                    <SiMysql className="hover:text-mysqlcolor duration-300"/>
                    <SiPrisma className="hover:text-prismacolor duration-300"/>
                </div>
            </div>
        </>

    );
}