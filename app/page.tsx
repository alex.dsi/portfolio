import NavBar from "@/components/navigation";
import Presentation from "@/components/home";
import Footer from "@/components/footer";
import Stack from "@/components/stack";
import Project from "@/components/projects";
import Form from "@/components/contact";

export default function Home() {
  return (
        <main className="xl:mx-24 md:mx-10 mx-6 mt-16 md:mt-24 sm:mx-4 flex flex-col gap-20 ">
            <Presentation />
            <Stack />
            <Project />
            <Form />
        </main>



  )
}
