import { FaArrowRight } from "react-icons/fa";
import Link from "next/link";

export default function Project() {
    const elExperience = [
        {
            id: 1,
            titre: "Stages CFP",
            Description:
                "Il s'agit d'un site où les professeurs d'université postent des demandes de stage auxquelles les étudiants peuvent répondre. J'ai participé à l'ensemble du projet",
            techno: ["Symfony", "MySQL", "Foundation UI"],
            link: "https://github.com/alexdissi/StageCFP",
        },
        {
            id: 2,
            titre: "Quick QR",
            Description:
                "J'ai créé une plateforme qui permet de creer des codes QR instantanément pour tout type de lien sans tracker l'utilisateur et sans publicité.",
            techno: ["NextJS", "API", "TailWind"],
            link: "https://quick-qr-gamma.vercel.app/",
        },
        {
            id: 3,
            titre: "Gret@ Distance",
            Description:
                "J'ai créé une plateforme d'apprentissage à distance qui permet aux étudiants de suivre différents cours sous différents formats (vidéo, texte, PDF).",
            techno: ["Symfony", "TailWind", "MySQL", "API"],
            link: "https://github.com/alexdissi/GretaDistance",
        },
    ];

    return (
        <>
            <h2 id="projects" className="font-sans text-center font-extrabold text-3xl text-solidheadinglight dark:text-solidheadingdark">
       Mes Projets
            </h2>
            <div className="flex flex-wrap justify-center md:flex-row gap-6 mx-auto">
                {elExperience.map((experience) => (
                    <div className="rounded-2xl shadow-sm " key={experience.id}>
                        <div
                            className="group overflow-hidden relative after:duration-500 before:duration-500  duration-500 hover:after:duration-500 hover:after:translate-x-24 hover:before:translate-y-12 hover:before:-translate-x-32 hover:duration-500 after:absolute after:w-24 after:h-24 after:bg-pink-600 after:rounded-full  after:blur-xl after:bottom-32 after:right-16 after:w-12 after:h-12  before:absolute before:w-20 before:h-20 before:bg-sky-400 before:rounded-full  before:blur-xl before:top-20 before:right-16 before:w-12 before:h-12  hover:scale-110 flex justify-center items-center h-56 w-80 bg-neutral-300 dark:bg-neutral-900 rounded-2xl outline outline-slate-400 -outline-offset-8">
                            <div className="z-10 flex flex-col items-center gap-2">
                                <span className="text-slate-950 dark:text-slate-400 text-xl font-bold">{experience.titre} </span>
                                <p className="text-sm mx-5 text-center">{experience.Description}</p>
                                <Link className="mt-2" href={experience.link} target="_blank"><FaArrowRight/></Link>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </>

    );
}