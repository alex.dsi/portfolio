import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import './globals.css'
import {ThemeProvider} from "@/components/theme-provider";
import NavBar from "@/components/navigation";
import Footer from "@/components/footer";
import { GeistSans } from 'geist/font/sans'
import { GeistMono } from 'geist/font/mono'
import {Analytics} from "@vercel/analytics/react";
import {SpeedInsights} from "@vercel/speed-insights/next";

export const metadata: Metadata = {
  title: 'Alexandre Dissi',
  description: 'Portfolio du développeur web Alexandre Dissi',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en" className={`${GeistSans.variable} ${GeistMono.variable}`}>
      <body className="max-w-full">

      <ThemeProvider attribute="class"
                     defaultTheme="system"
                     enableSystem
                     disableTransitionOnChange>
          <header>
              <NavBar/>
          </header>
          {children}
          <footer>
              <Footer/>
          </footer>
      </ThemeProvider>
      <SpeedInsights />
      <Analytics />
      </body>
    </html>
  )
}
