"use client";
import Typewriter from "typewriter-effect";
import Link from "next/link";
import {useState} from "react";

export default function Presentation() {
    const [isHover, setIsHover] = useState(false);
    return (
        <>
        <div className="flex flex-col-reverse gap-10 sm:flex-row items-center sm:justify-between">
            <div className="flex flex-col">
                <h1 className="scroll-m-20 text-3xl text-center sm:text-left font-extrabold lg:text-6xl">
                    Bonjour 👋🏼 ,<br/>
                    Je suis
                    <span
                        className="text-sky-500 hover:text-pink-600 duration-500 cursor-pointer"> Alexandre Dissi,</span>
                    <br/>
                    <div className="w-full">
                        <Typewriter
                            options={{
                                delay: 50,
                                loop: true,
                            }}
                            onInit={(typewriter) => {
                                typewriter

                                    .typeString("Un Developpeur Fullstack")
                                    .pauseFor(1000)
                                    .deleteAll()
                                    .typeString("Un Developpeur Front-End")
                                    .pauseFor(1000)
                                    .deleteAll()
                                    .typeString("Un Developpeur Back-End")
                                    .pauseFor(1000)
                                    .deleteAll()
                                    .start();
                            }}
                        />
                    </div>
                </h1>
            </div>
            <div className="relative group w-52 h-52 sm:w-80 sm:h-80 md:w-80 md:h-80"
                 onMouseEnter={() => setIsHover(true)}
                 onMouseLeave={() => setIsHover(false)}>

                <div
                    className="absolute w-full h-full max-w-[250px] max-h-[250px] md:max-w-[350px] md:max-h-[350px] rounded-full blur-3xl bg-gradient-to-br from-blue-500 via-fuchsia-500 to-pink-500"></div>

                <img src="me.png" alt="Profil picture of Alexandre Dissi"
                     className={`absolute w-full h-full rounded-full transition-transform duration-500 ease-in-out ${isHover ? 'rotate-y-180' : ''}`}/>

                <img src="me2.jpeg" alt="Hover Image"
                     className={`hidden group-hover:block absolute w-full h-full rounded-full transition-transform duration-500 ease-in-out ${isHover ? 'rotate-y-0' : 'rotate-y-180'}`}/>
            </div>
        </div>

    <div>
        <p className="text-justify font-sans font-medium">
            Bonjour, je m&apos;appelle Alexandre Dissi, j&apos;ai 25 ans et je
            suis développeur web. J&apos;ai suivi deux formations, l&apos;une de
            développeur web et l&apos;autre de concepteur-développeur
            d&apos;applications, qui m&apos;ont permis d&apos;acquérir un large
            éventail de compétences. Ce portfolio met en avant mes compétences en
            matière de design et de développement front-end. N&apos;hésitez pas à
            explorer mon portfolio pour découvrir mes projets et mes réalisations.
            J&apos;ai travaillé sur diverses technologies web telles que
            JavaScript, PHP, GO, NodeJS et des frameworks comme React, Next JS,
            VueJS, Symfony et Laravel. J&apos;accorde une grande importance à
            l&apos;expérience utilisateur et à la création d&apos;interfaces
            esthétiques et conviviales. Si vous avez des questions ou souhaitez
                    discuter de collaborations potentielles, n&apos;hésitez pas à me
                    contacter. Je suis ouvert aux opportunités de travail et toujours prêt
                    à relever de nouveaux défis. Je vous remercie de votre visite et de
                    l&apos;intérêt que vous portez à mon travail.
                    <br/>
                    <Link href={"/CV.pdf"} target="_blank" className="underline">
                        Voir mon CV
                    </Link>
                </p>
                <div className="flex flex-row mt-6 items-center gap-6 ">
                    <div className="bg-green-500 sm:w-4 w-8 h-4 rounded-full pulse-animation"></div>
                    <span className="text-primary font-sans font-medium">
            Status : Developpeur FullStack chez <a className='underline' target="_blank" href="https://www.cashnowmobile.com">CashNowMobile</a> depuis Novembre 2023
          </span>
                </div>
            </div>


        </>
    );
}