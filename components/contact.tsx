'use client';
import { createClient } from "@supabase/supabase-js";
import { exit } from "process";
import { useState } from "react";
import {Input} from "@/components/ui/input";
import {Textarea} from "@/components/ui/textarea";
import {Label} from "@/components/ui/label";
import {Button} from "@/components/ui/button";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const supabaseUrl = process.env.NEXT_PUBLIC_SUPABASE_URL;
const supabaseKey = process.env.NEXT_PUBLIC_SUPABASE_API_KEY;
let supabase: any;

if (supabaseUrl && supabaseKey) {
    supabase = createClient(supabaseUrl, supabaseKey);
}

export default function Form() {
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [message, setMessage] = useState("");
    const [isAlertVisible, setIsAlertVisible] = useState(false);

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        // Vérification des champs requis
        if (!name || !email || !message) {
            alert("Veuillez remplir tous les champs");
            return;
        }

        // Enregistrement des données dans la table "contact" de Supabase
        const { data, error } = await supabase
            .from("contact")
            .insert([{ name, email, message }]);

        if (error) {
           toast.error("Une erreur s'est produite lors de l'enregistrement de votre message. Veuillez réessayer.");
            exit(
            );
        } else {
            setIsAlertVisible(true);
            // Réinitialiser les champs du formulaire après l'enregistrement
            setName("");
            setEmail("");
            setMessage("");
            toast.success(
                "Votre message a bien été envoyé ! Je vous répondrai dès que possible."
            );
        }
    };

    return (
        <div
            className="flex flex-col items-center w-full gap-10"
        >
            <h2 id="contact"
                className="font-sans text-center font-extrabold text-3xl text-solidheadinglight dark:text-solidheadingdark">
               Contacte Moi
            </h2>

            <div className="flex flex-col md:flex-row items-center gap-5 md:gap-44">
                <div className="w-80 sm:w-[500px]">
                    <h3 className="text-md text-justify md:text-5xl font-sans font-extrabold  dark:text-solidheadingdark mt-3">
                        Contacte moi ! Je serais heureux de discuter avec toi
                    </h3>
                </div>

                <div>
                    <form
                        onSubmit={handleSubmit}
                        action=""
                        className="flex flex-col gap-8 md:w-[30rem] w-80 justify-between"
                    >
                        <div>
                            <Label htmlFor="name">Nom :</Label>
                            <Input type="text"
                                   name={"name"}
                                   value={name}
                                   onChange={(e) => setName(e.target.value)}
                                   placeholder="Nom"
                            />
                        </div>

                        <div>
                            <Label htmlFor="email">Email :</Label>
                            <Input type="email"
                                    name={"email"}
                                   value={email}
                                   onChange={(e) => setEmail(e.target.value)}
                                   placeholder="Email"
                            />
                        </div>

                        <div>
                            <div className="grid w-full gap-1.5">
                                <Label htmlFor="message">Votre Message :</Label>
                                <Textarea name={message} placeholder="Votre message ici" value={message}
                                          onChange={(e) => setMessage(e.target.value)} id="text"/>
                            </div>
                            <Button type="submit" className="float-right mt-4" disabled={!name || !email || !message}>Envoyer</Button>
                        </div>
                    </form>
                </div>
            </div>
            <ToastContainer />
        </div>
    );
}